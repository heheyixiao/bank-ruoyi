package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.Balance;
import com.ruoyi.system.domain.vo.BalanceVo;
import com.ruoyi.system.mapper.BankBalanceMapper;
import com.ruoyi.system.service.IBankBalanceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class BankBalanceServiceImpl implements IBankBalanceService {

    @Autowired
    BankBalanceMapper mapper;


    @Override
    public Balance getBalanceByUserId(Long userId) {
        return mapper.getBalanceByUserId(userId);
    }

    @Override
    public Balance updateBalance(BalanceVo balanceVo) {

        Balance balance = new Balance();

        BeanUtils.copyProperties(balanceVo,balance);

        Boolean save = balanceVo.getSave();
        if (ObjectUtils.isEmpty(save)){
            throw new RuntimeException("参数非法");
        }

        if (save){
            balance.setBalance(balanceVo.getBalance().add(balanceVo.getValue()));
        } else {
            if (balanceVo.getBalance().compareTo(balanceVo.getValue())<0){
                throw new RuntimeException("取钱金额超过用户余额");
            }
            balance.setBalance(balanceVo.getBalance().subtract(balanceVo.getValue()));
        }
        mapper.updateBalance(balance);
        return balance;
    }

    @Override
    public int initUserBalance(Long userId) {
        return mapper.initUserBalance(userId);
    }
}
