package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.Balance;

import java.math.BigDecimal;

public class BalanceVo extends Balance {
    private Boolean save;
    private BigDecimal value;

    public Boolean getSave() {
        return save;
    }

    public void setSave(Boolean save) {
        this.save = save;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString() + " BalanceVo{" +
                "save=" + save +
                ", value=" + value +
                '}';
    }
}
