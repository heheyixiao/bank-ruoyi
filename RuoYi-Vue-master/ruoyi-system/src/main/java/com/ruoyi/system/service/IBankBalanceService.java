package com.ruoyi.system.service;

import com.ruoyi.system.domain.Balance;
import com.ruoyi.system.domain.vo.BalanceVo;

public interface IBankBalanceService {

    Balance getBalanceByUserId(Long userId);

    Balance updateBalance(BalanceVo balance);

    int initUserBalance(Long user);

}
