package com.ruoyi.system.domain;


import java.math.BigDecimal;


public class Balance {

  private Long id;
  private Long userId;
  private BigDecimal balance;

  public Long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public Long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }


  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  @Override
  public String toString() {
    return "Balance{" +
            "id=" + id +
            ", userId=" + userId +
            ", balance=" + balance +
            '}';
  }
}
