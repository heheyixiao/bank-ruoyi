package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Balance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BankBalanceMapper {

    Balance getBalanceByUserId(Long userId);

    int updateBalance(Balance balance);

    int initUserBalance(Long userId);

}
