import request from '@/utils/request'

export function getBalance(userId) {
  return request({
    url: '/bank/balance/' + userId,
    method: 'get'
  })
}

export function updateBalance(data) {
  return request({
    url: '/bank/balance/',
    method: 'put',
    data: data
  })
}
