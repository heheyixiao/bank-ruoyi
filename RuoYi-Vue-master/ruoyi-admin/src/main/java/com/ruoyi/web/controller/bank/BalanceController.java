package com.ruoyi.web.controller.bank;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.vo.BalanceVo;
import com.ruoyi.system.service.IBankBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 用户信息
 *
 * @author xxn
 */
@RestController
@RequestMapping("/bank/balance")
public class BalanceController extends BaseController {

    @Autowired
    IBankBalanceService service;

    @GetMapping("/{userId}")
    public AjaxResult getBalance(@PathVariable Long userId) {
        return AjaxResult.success(service.getBalanceByUserId(userId));
    }

    @PutMapping("/")
    public AjaxResult updateBalance(@RequestBody BalanceVo balanceVo) {
        return AjaxResult.success(service.updateBalance(balanceVo));
    }
}
