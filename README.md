# bank-ruoyi

#### 介绍
Bank demo based on ruoyi

#### 软件架构
基于若依开发银行demo


#### 安装教程

1. 修改nginx.conf
```shell
location /prod-api/{
         proxy_set_header Host $http_host;
         proxy_set_header X-Real-IP $remote_addr;
         proxy_set_header REMOTE-HOST $remote_addr;
         proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_pass 修改为后端地址,如果后端在本机使用本机ip;
      }
```
2. 修改数据库地址，在docker-compose.yml修改环境变量
```shell
bank-admin:
    container_name: bank-admin
    environment:
      MYSQL_ADDR: 访问地址,可能是ip:port,也可能是云数据库地址
      MYSQL_USER: 有权限的用户,默认root
      MYSQL_PWD: 用户对应密码,默认123456
    build:
      context: ./bank
      dockerfile: dockerfile
    ports:
      - "8080:8080"
    depends_on:
      - bank-mysql
      - bank-nginx
```
3. 使用docker-compose启动
```shell
# 编译启动
docker-compose up -d --build
# 查看启动进程
docker ps -a
# 查看日志
docker logs -f --tail 100 镜像id
```

4. 停止时建议删除所有container和images
```shell
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi 镜像Id
```
5. 根据资料可以使用docker-compose down命令停止，没有测试，可以自行测试并修改
